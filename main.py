import logging, os, random
from telethon import TelegramClient, events
from dotenv import load_dotenv
from match_score import generate_match_score


logging.basicConfig(filename="debug.log", format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

load_dotenv()

API_ID = os.getenv('API_ID')
API_HASH = os.getenv('API_HASH')
LIKE_ONCE = os.getenv('LIKE_ONCE')
WORDS = os.getenv('WORDS')
SCORE_LIMIT = int(os.getenv('SCORE_LIMIT'))


###########
LIKE_SYMBOL = '❤️'
DISLIKE_SYMBOL = '👎'
OK_SYMBOL = "👍Полезно"
NEXT_SYMBOL = "В другой раз"
READY_SYMBOL = "Готово"
LATER_SYMBOL = "Возможно позже"
OK_BUTTON_SYMBOL = "Продолжить просмотр анкет"


ALIST = ["1", "👍Полезно", "В другой раз", "Возможно позже", "Продолжить просмотр анкет", "Анкеты в Telegram"]

#########
group_id = None

client = TelegramClient('anon', API_ID, API_HASH)

USERS = []

@client.on(events.NewMessage)
async def event_handler(event):
    try:
        global group_id
        chat = await event.get_chat()

        username = chat.username
        if username == "leomatchbot":
            message = event.message
            if message.out == True:
                pass
            else:

                if LIKE_ONCE and message.message in USERS:
                    logger.info("already liked" + message.message)
                    await event.reply( LIKE_SYMBOL if random.randint(0, 1) == 0 else DISLIKE_SYMBOL)
                elif message.message == "Нет такого варианта ответа":
                    await event.reply("1")
                else:
                    logger.info(message.message)
                    if event.grouped_id == None:
                        group_id = None

                        ## new normal message
                        
                        button_count = message.button_count

                        if button_count == 1:
                            event.reply(message.buttons[0].text)
                            
                        elif button_count == 2:
                            if message.buttons[0].text in ALIST:
                                await event.reply(message.buttons[0].text)
                            elif message.buttons[1].text in ALIST:
                                await event.reply(message.buttons[1].text)
                            else:
                                r = random.randint(0, 1)
                                await event.reply(message.buttons[r].text)
                        else:

                            message_text = message.message if message.message != None else ""
                            score = generate_match_score(WORDS, message_text)
                            
                            if score > SCORE_LIMIT:
                                await event.reply(LIKE_SYMBOL)
                            else:
                                await event.reply(DISLIKE_SYMBOL)
                            USERS.append(message.message)

                    elif event.grouped_id != None and group_id != event.grouped_id:
                        ## new multi photo message
                        group_id = event.grouped_id
                        message_text = message.message

                        score = generate_match_score(WORDS, message_text)
                        if score > SCORE_LIMIT:
                            await event.reply(LIKE_SYMBOL)
                        else:
                            await event.reply(DISLIKE_SYMBOL)
                        USERS.append(message.message)
                    else:
                        pass
    except Exception as err:
        logger.error(err)
        
if __name__ == "__main__":

    try:
        client.start()
        client.run_until_disconnected()
    except KeyboardInterrupt:
        with open('users.txt', 'a') as f:
            for user in USERS:
                f.write(user + '\n')
            f.close()
        f.close
