import numpy as np
import jellyfish



def calculate_bio_similarity(bio1, bio2):
    
    try:
        distance = jellyfish.levenshtein_distance(bio1.lower(), bio2.lower())
        max_length = max(len(bio1), len(bio2))
        similarity_score = 1 - (distance / max_length)
        return similarity_score
    except Exception as err:
        print(err)

        
def generate_match_score(user1_bio, user2_bio):
    try:
        bio_similarity_score = calculate_bio_similarity(user1_bio, user2_bio)
        match_score = np.mean([bio_similarity_score])
        return match_score * 10
    except Exception as err:
        print(err)


